package main;

public class Main {
    //TODO: Write a program to use this system on(Needs to have single next line statements and comments)
    //TODO: Documentation
    //TODO: Testing
    public static void main(String[] args) {
        JavaFile file = null;
        String savePath = null;
        if(args.length > 0) {
            if(args[0].toLowerCase().endsWith(".java")) {
                file = new JavaFile(args[0]);
                if(args.length > 1 && args[1].toLowerCase().endsWith(".java")) savePath = args[1];
            }
        }

        if(file != null) {
            file.indentFile();
            file.printProgram();
            if(savePath != null) file.saveToFile(savePath);
        }
    }

    /**
     * To check whether a specific character is in a String to be used when checking comments and braces
     * @param code - The code to check
     * @param c - The character to check
     * @return Whether the specified character(c) is in a String
     */
    public static boolean isInString(String code, int c) {
        int quotes = 0;
        if(c <= code.length()) {
            if((code.contains("\"") && code.indexOf('"') <= c)) {
                for(int i = 0; i < c; i++) {
                    if(code.charAt(i) == '"') quotes++;
                }
            }
        }
        return quotes % 2 != 0; //Odd number
    }
}