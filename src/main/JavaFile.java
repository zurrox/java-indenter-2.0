package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class JavaFile {
    private ArrayList<JavaLine> lines = new ArrayList<>();
    private String[] keywords = {"for", "while", "if", "else"};

    public JavaFile(String filePath) {
        readFile(Paths.get(filePath.replace("\\", "\\\\")));
    }

    /**
     * Reads the file input from program arguments into the lines ArrayList
     * @param filePath - The path of the file to read
     */
    private void readFile(Path filePath) {
        try {
            lines.addAll(Files.readAllLines(filePath).stream().map(JavaLine::new).collect(Collectors.toList())); //IntelliJ Generated Method Referencing
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the code from the lines ArrayList into the specified file
     * @param filePath - The new file path to save the indented code to
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void saveToFile(String filePath) {
        try {
            Path newFile = Paths.get(filePath.replace("\\", "\\\\"));
            if(newFile.toFile().exists()) Files.delete(newFile);
            File file = newFile.toFile();
            if(!newFile.getParent().toFile().exists()) newFile.getParent().toFile().mkdirs();
            if(!newFile.toFile().exists()) newFile.toFile().createNewFile();
            FileWriter fw = new FileWriter(file);
            for(JavaLine line : lines) fw.write(line.getLine() + "\n");
            fw.flush();
            fw.close();
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Finds the longest line of code to indent all comments to be after
     * @return The length of the longest line of code in the file
     */
    private int getLongestLine() {
        int max = 0;
        for(JavaLine line : lines) {
            if(line.getCode().length() > max) max = line.getCode().length();
        }
        return max;
    }

    /**
     * Print's the whole program into the console from the lines ArrayList
     */
    public void printProgram() {
        for(JavaLine line : lines) System.out.println(line.getLine());
    }

    /**
     * Indents comments and braces in the file(Stored in the lines ArrayList)
     */
    public void indentFile() {
        indentBraces();
        indentComments();
    }

    /**
     * The amount of open braces over close braces to calculate indents
     * @param s - The code for the braces to be counted in
     * @return The amount of braces to open or close
     */
    private int countBraces(String s) {
        int braces = 0;
        if(s.contains("{") || s.contains("}")) {
            for(int i = 0; i < s.length(); i++) {
                if(s.charAt(i) == '{' && !Main.isInString(s, i)) braces++;
                else if(s.charAt(i) == '}' && !Main.isInString(s, i)) braces--;
            }
        }
        return braces;
    }

    /**
     * Whether the line input contains a construct keyword(if, for, else, while)
     * @param line - The line to check
     * @return Whether a keyword from the keyword array is in the current line
     */
    private boolean containsKeyword(String line) {
        for(String word : keywords) if(line.contains(word)) return true;
        return false;
    }

    /**
     * Indents all methods and code inside of braces by 2 lines
     */
    private void indentBraces() {
        int braces = 0, tempBraces = 0;
        for(JavaLine line : lines) {
            line.setCode(line.getCode().trim()); //Removes leading and starting spaces
            String code = line.getCode();
            int currentBraces = countBraces(code);
            braces += currentBraces;
            if(currentBraces > 0 && tempBraces > 0) tempBraces--;

            String tempCode = code.trim();
            //Checks for the current line
            if(currentBraces == 0 && tempCode.startsWith("}")) tempBraces -= 1;

            if(currentBraces > 0) {
                line.indentLine((braces - currentBraces + tempBraces) * 2);
            }else line.indentLine((braces + tempBraces) * 2);
            tempBraces = 0;
            //Checks for the next line
            if(currentBraces == 0 && tempCode.endsWith("{")) tempBraces += 1;

            if(containsKeyword(tempCode)) {
                tempCode = tempCode.substring(tempCode.indexOf(')') + 1);
                if(!tempCode.contains("{")) {
                    tempBraces += 1;
                }else {
                    while(tempCode.contains("{")) {
                        int braceIndex = tempCode.indexOf("{");
                        if(Main.isInString(tempCode, braceIndex)) tempCode = tempCode.substring(tempCode.indexOf('{'));
                        else break;
                    }
                    if(tempCode.contains("{")) tempBraces += 1;
                }
            }
        }
    }

    /**
     * Indents all comments to be at the same line
     */
    private void indentComments() {
        int longestLine = getLongestLine();
        for(JavaLine line : lines) {
            line.indentComment(longestLine - line.getCode().length());
        }
    }
}