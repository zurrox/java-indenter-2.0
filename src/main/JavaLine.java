package main;

public class JavaLine {
    private String code, comment;

    public JavaLine(String line) {
        line = line.replace("\t", "    ");
        splitCommentCode(line);
        code = code.trim();
    }

    /**
     * Splits the code from the comment excluding the //
     * @param line - The line of code to split
     */
    private void splitCommentCode(String line) {
        if(line.contains("//")) {
            char lastChar = ' ';
            for(int i = line.indexOf("//"); i < line.length(); i++) {
                if(line.charAt(i) == '/' && lastChar == '/' && !Main.isInString(line, i)) {
                    code = line.substring(0, i - 1);
                    comment = line.substring(i + 1); //i should be at the second / so add one to just get the comment
                    return;
                }
                lastChar = line.charAt(i);
            }
        }
        code = line;
    }

    /**
     * Whether there is a comment in this line
     * @return If the line contains a comment
     */
    public boolean hasComment() {
        return comment != null && !comment.isEmpty();
    }

    /**
     * The code in the line
     * @return The code of this line
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code of this line
     * @param code - The code to replace
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Indents the code by the specified amount
     * @param spaces - The amount of spaces to indent before the code
     */
    public void indentLine(int spaces) {
        for(int i = 0; i < spaces; i++) code = " " + code;
    }

    /**
     * Indents the comment by the specified amount
     * @param spaces - The amount of spaces before the comment
     */
    public void indentComment(int spaces) {
        for(int i = 0; i < spaces && hasComment(); i++) code += " ";
    }

    /**
     * The line including indentation and the comment
     * @return The whole line formatted with the // and indentation
     */
    public String getLine() {
        return code + (hasComment() ? "//" + comment : "");
    }
}